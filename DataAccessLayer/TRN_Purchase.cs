//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DataAccessLayer
{
    using System;
    using System.Collections.Generic;
    
    public partial class TRN_Purchase
    {
        public int PurchaseId { get; set; }
        public Nullable<int> RefBillId { get; set; }
        public string Barcode { get; set; }
        public int RefItemId { get; set; }
        public string BrandName { get; set; }
        public string ModelName { get; set; }
        public string Particulars { get; set; }
        public decimal PurchaseRate { get; set; }
        public Nullable<decimal> SalesRate { get; set; }
        public int Quantity { get; set; }
        public int RemainingQnt { get; set; }
        public int Free { get; set; }
        public int RemFree { get; set; }
        public decimal PurchaseTaxOnMRP { get; set; }
        public decimal PurchaseTaxOnRate { get; set; }
        public decimal DiscountAmount { get; set; }
        public Nullable<decimal> MRP { get; set; }
        public string BatchNo { get; set; }
        public Nullable<System.DateTime> ExpiryDate { get; set; }
        public string CreatedBy { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public string UpdatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedOn { get; set; }
        public bool Isactive { get; set; }
    
        public virtual MSTR_Item MSTR_Item { get; set; }
        public virtual TRN_PurchaseBillDetails TRN_PurchaseBillDetails { get; set; }
    }
}
